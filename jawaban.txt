//soal no 1
CREATE DATABASE myshop;

//soal no 2
CREATE TABLE myshop.users ( 
	id INTEGER NOT NULL AUTO_INCREMENT ,
	name VARCHAR(255) NOT NULL ,
	email VARCHAR(255) NOT NULL ,
	password VARCHAR(255) NOT NULL ,
	PRIMARY KEY (id)
);

CREATE TABLE myshop.categories ( 
	id INT NOT NULL AUTO_INCREMENT ,
	name VARCHAR(255) NOT NULL ,
	PRIMARY KEY (id)
);

CREATE TABLE myshop.items ( 
	id INT NOT NULL AUTO_INCREMENT ,
	name VARCHAR(255) NOT NULL ,
	description VARCHAR(255) NOT NULL ,
	price INT NOT NULL ,
	stock INT NOT NULL ,
	category_id INT NOT NULL ,
	PRIMARY KEY (id),
	FOREIGN KEY (category_id) REFERENCES categories(id)
);

//soal no 3
INSERT INTO users (name, email, password) VALUES 
('John Doe', 'john@doe.com', 'john123'), 
('Jane Doe', 'jane@doe.com', 'jenita123');

INSERT INTO categories (name) VALUES 
('gadget'), 
('cloth'), 
('men'), 
('women'), 
('branded');

INSERT INTO items (name, description, price, stock, category_id) VALUES 
('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1), 
('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2), 
('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);

//soal no 4
//a) Mengambil data users
SELECT id, name, email FROM users;

//b1) Mengambil items harga > 1000000
SELECT * FROM items WHERE price > 1000000;

//b2) Mengambil data yang like "uniklo", "watch" atau "sang"
SELECT * FROM items WHERE name like '%uniklo%';

//c) Menampilkan data items join dengan kategori
SELECT i.name, i.description, i.price, i.stock, i.category_id, c.name as kategori
FROM items as i JOIN categories as c ON i.category_id=c.id;

//soal no 5
UPDATE items
SET price = 2500000
WHERE name like 'Sumsang%';